import { find, without } from 'lodash'
import http from '../services/http'
import stub from '../stubs/user'

export const userStore = {
  stub,

  state: {
    current: stub
  },

  /**
   * Init the store.
   *
   * @param {Object}      currentUser The current user.
   */
  init (currentUser) {
    this.current = currentUser
  },

  clean () {
    this.state.current = stub
  },

  /**
   * All users.
   *
   * @return {Array.<Object>}
   */
  get all () {
    return this.state.users
  },

  /**
   * Set all users.
   *
   * @param  {Array.<Object>} value
   */
  set all (value) {
    this.state.users = value
  },

  /**
   * Get a user by his ID
   *
   * @param  {Integer} id
   *
   * @return {Object}
   */
  byId (id) {
    return find(this.all, { id })
  },

  /**
   * The current user.
   *
   * @return {Object}
   */
  get current () {
    return this.state.current
  },

  /**
   * Set the current user.
   *
   * @param  {Object} user
   *
   * @return {Object}
   */
  set current (user) {
    this.state.current = user
    return this.state.current
  },

  /**
   * Log a user in.
   *
   * @param  {String}   email
   * @param  {String}   password
   */
  login (email, password) {
    return new Promise((resolve, reject) => {
      http.post('login', { email, password }, ({ data }) => {
        resolve(data)
      }, error => reject(error))
    })
  },

  /**
   * Log the current user out.
   */
  logout () {
    return new Promise((resolve, reject) => {
      http.delete('me', {}, ({ data }) => {
        resolve(data)
      }, error => reject(error))
    })
  },

  /**
   * Update the current user's profile.
   *
   * @param  {string} password Can be an empty string if the user is not changing his password.
   */
  updateProfile (user) {
    return new Promise((resolve, reject) => {
      http.put('me', {user: user}, (user) => {
        resolve(this.current)
      },
      error => reject(error))
    })
  },

  /**
   * Stores a new user into the database.
   *
   * @param  {user}   user
   */
  store (user) {
    return new Promise((resolve, reject) => {
      http.post('users', { user: user }, ({ data }) => {
        resolve(data)
      }, error => reject(error))
    })
  },

  /**
   * Update a user's profile.
   *
   * @param  {Object}   user
   * @param  {String}   name
   * @param  {String}   email
   * @param  {String}   password
   */
  update (user) {
    return new Promise((resolve, reject) => {
      http.put(`users/${user.id}`, { user: user }, ({ user }) => {
        resolve(user)
      }, error => reject(error))
    })
  },

  /**
   * Delete a user.
   *
   * @param  {Object}   user
   */
  destroy (user) {
    return new Promise((resolve, reject) => {
      http.delete(`user/${user.id}`, {}, ({ data }) => {
        this.all = without(this.all, user)

        // Mama, just killed a man
        // Put a gun against his head
        // Pulled my trigger, now he's dead
        // Mama, life had just begun
        // But now I've gone and thrown it all away
        // Mama, oooh
        // Didn't mean to make you cry
        // If I'm not back again this time tomorrow
        // Carry on, carry on, as if nothing really matters
        //
        // Too late, my time has come
        // Sends shivers down my spine
        // Body's aching all the time
        // Goodbye everybody - I've got to go
        // Gotta leave you all behind and face the truth
        // Mama, oooh
        // I don't want to die
        // I sometimes wish I'd never been born at all

        /**
         * Brian May enters the stage.
         */
        resolve(data)
      }, error => reject(error))
    })
  }
}
