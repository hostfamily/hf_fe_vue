import { userStore } from './user'
import http from '../services/http'
import jwt from '../services/jwt'
import { assign } from 'lodash'

export const sharedStore = {
  state: {
    user: null,
    data: null
  },

  async init () {
    await this.initUserStore()
  },

  async initUserStore () {
    return new Promise((resolve, reject) => {
      http.get('users/' + jwt.getValueFromKey('user_id'), ({ data }) => {
        assign(this.state, data)
        userStore.init(this.state.user)
        resolve(this.state)
      }, error => reject(error))
    })
  },

  clean () {
    userStore.clean()
  }
}
