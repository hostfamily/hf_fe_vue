export default {
  educational_background: {
    filled: true
  },
  about_me: {
    filled: true
  },
  health_lifestyle: {
    filled: true
  },
  my_description: {
    filled: true
  },
  passport_info: {
    filled: true
  },
  program_preference: {
    filled: true
  },
  uncompleted_sections: []
}
