export default {
  type: '',
  firstname: '',
  lastname: '',
  email: '',
  email_confirmation: '',
  password: '',
  password_confirmation: '',
  birthday: '',
  gender: '',
  state: '',
  city: '',
  address: ''
}
