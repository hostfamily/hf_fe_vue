// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

function login(browser, email, password) {
  const devServer = browser.globals.devServerURL;

  return browser
    .url(devServer)
    .waitForElementVisible('#app', 1000)
    .setValue('input[name=login_mail]', email)
    .setValue('input[name=login_password]', password)
    .click('button[name=login]')
    .pause(1000)
}

module.exports = {
  'login should fail': function test(browser) {
      login(browser, 'djaoisfjdois', 'jodsajoifjdoisajfoi')
      .assert.visible('.activate_acc.act_acc_open')
      .assert.containsText('.activate_acc.act_acc_open', 'Invalid Username/Password')
      .click('.activate_acc.act_acc_open')
      .pause(1000)
      .assert.hidden('.activate_acc')
      .end()
  },

  'signup': function test(browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL;

    browser
      .url(devServer)
      .waitForElementVisible('#app', 2000)
      .setValue('input[name=exptype]', 'student')
      .setValue('input[name=firstname]', 'Miao')
      .setValue('input[name=lastname]', 'Miao')
      .setValue('input[name=email]', 'iol@io.it')
      .setValue('input[name=email_confirmation]', 'iol@io.it')
      .setValue('input[name=password]', 'Miao1.')
      .setValue('input[name=password_confirmation]', 'Miao1.')
      .setValue('input[name=birthday]', '05/06/1995')
      .setValue('input[name=gender]', 'M')
      .setValue('input[name=state]', 'Italy')
      .setValue('input[name=city]', 'Terni')
      .setValue('input[name=address]', 'Via Montanara 8')
      .click('button[name=signup]')
      .pause(2000)
      .assert.visible('article.is-success')
  },

  'educational background filling and submit': function test(browser) {
    const devServer = browser.globals.devServerURL;

    login(browser, 'a@stud.it', 'Miao1.')
    browser
      .url(devServer + '/home/educational_background')
      .setValue('input[name=school]', 'Harvard University')
      .setValue('input[name=from]', '10-09-2002')
      .setValue('input[name=to]', '18-04-2007')
      .setValue('textarea[name=description]', 'That was an awesome experience')
      .setValue('input[name=graduated]', true)
      .setValue('input[name=mother_language]', 'Italian')
      .click('button[name=submit]')
      .pause(2000)
      .assert.visible('article.is-success')
      .end()
  }

/*
  'hello': function test(browser) {
    const devServer = browser.globals.devServerURL;
    browser.url('google.com')
  }
  */
};
