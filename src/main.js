// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VeeValidate from 'vee-validate'

import App from './App'
import router from './router'
import http from './services/http'
import { event } from './utils'
import VueQuillEditor from 'vue-quill-editor'
import VueGmaps from 'vue-gmaps'

Vue.config.productionTip = false

const veeValidateConfig = { delay: 4 }

Vue.use(VeeValidate, veeValidateConfig)
Vue.use(VueQuillEditor)

Vue.use(VueGmaps, {
  key: 'AIzaSyDIoNPZPJzZIU6Bu2T9N93ts3RD1LR-xs0'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  created () {
    event.init()
    http.init()
  }
})
