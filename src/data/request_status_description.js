export default {
  'no-request': 'You can send your request when you will fill all forms.',
  'pending': 'Your request has been sent to a volunteer to get approved.',
  'rejected': 'Your request has rejected by a volunteer.',
  'accepted': 'Congratulations your request has been accepted, a volunteer will associate you soon.',
  'associate': 'You has been associated. You should have received a mail with the association response.'
}
