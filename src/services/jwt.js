import jwtDecode from 'jwt-decode'
import { ls } from './ls'

export default {
  getDecodedToken () {
    return jwtDecode(ls.get('jwt-token'))
  },

  getValueFromKey (key) {
    return jwtDecode(ls.get('jwt-token'))[key]
  }
}
