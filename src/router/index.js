import Vue from 'vue'
import Router from 'vue-router'
import Signup from '@/components/Signup'
import Home from '@/components/Home'
import AboutMe from '@/components/AboutMe'
import EducationalBackground from '@/components/EducationalBackground'
import HealthLifestyle from '@/components/HealthLifestyle'
import PassportInfo from '@/components/PassportInfo'
import MyDescription from '@/components/MyDescription'
import ProgramPreference from '@/components/ProgramPreference'
import NotFound from '@/components/NotFound'
import CreatePost from '@/components/CreatePost'
import Posts from '@/components/Posts'
import Dashboard from '@/components/Dashboard'
import MyJourneyRequest from '@/components/MyJourneyRequest'
import CreateEvent from '@/components/CreateEvent'
import Events from '@/components/Events'
import ShowPost from '@/components/ShowPost'
import ManageRequests from '@/components/ManageRequests'
import ReviewUser from '@/components/ReviewUser'
import AssociateRequests from '@/components/AssociateRequests'

import { ls } from '../services/ls'

Vue.use(Router)

function isLogged () {
  return ls.get('jwt-token') !== null
}

function requireAuth (to, from, next) {
  if (isLogged()) {
    next()
  } else {
    next('/')
  }
}

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Signup',
      component: Signup,
      beforeEnter: (to, from, next) => {
        if (isLogged()) {
          next('/home')
        } else {
          next()
        }
      }
    },
    {
      path: '/post/:id',
      component: ShowPost
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      beforeEnter: requireAuth,
      redirect: '/home/dashboard',
      children: [
        {
          path: 'dashboard',
          component: Dashboard
        },
        {
          path: 'my_journey_request',
          component: MyJourneyRequest
        },
        {
          path: 'about_me',
          component: AboutMe
        },
        {
          path: 'educational_background',
          component: EducationalBackground
        },
        {
          path: 'health_lifestyle',
          component: HealthLifestyle
        },
        {
          path: 'passport_info',
          component: PassportInfo
        },
        {
          path: 'my_description',
          component: MyDescription
        },
        {
          path: 'program_preference',
          component: ProgramPreference
        },
        {
          path: 'create_post',
          component: CreatePost
        },
        {
          path: 'edit_post/:id',
          component: CreatePost,
          props: { action: 'edit' }
        },
        {
          path: 'posts',
          component: Posts
        },
        {
          path: 'create_event',
          component: CreateEvent
        },
        {
          path: 'events',
          component: Events
        },
        // Volunteer routes
        {
          path: 'host_families',
          component: ManageRequests,
          props: { type: 'host_families' }
        },
        {
          path: 'students',
          component: ManageRequests,
          props: { type: 'students' }
        },
        {
          path: 'review_user/:id',
          component: ReviewUser,
          redirect: 'review_user/:id/about_me',
          children: [
            {
              path: 'about_me',
              name: 'about_me',
              component: AboutMe,
              props: { dis: true }
            },
            {
              path: 'educational_background',
              name: 'educational_background',
              component: EducationalBackground,
              props: { dis: true }
            },
            {
              path: 'health_lifestyle',
              name: 'health_lifestyle',
              component: HealthLifestyle,
              props: { dis: true }
            },
            {
              path: 'passport_info',
              name: 'passport_info',
              component: PassportInfo,
              props: { dis: true }
            },
            {
              path: 'my_description',
              name: 'my_description',
              component: MyDescription,
              props: { dis: true }
            },
            {
              path: 'program_preference',
              name: 'program_preference',
              component: ProgramPreference,
              props: { dis: true }
            }
          ]
        },
        {
          path: 'associate_requests',
          component: AssociateRequests
        }
      ]
    },
    {
      path: '*',
      component: NotFound
    }
  ]
})
