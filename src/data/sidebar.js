export default {
  student: {
    'Rollbar': {
      'Home': 'dashboard',
      'My Journey Request': 'my_journey_request',
      'Posts': 'posts'
    },
    'My Infos': {
      'AboutMe': 'about_me',
      'Educational Background': 'educational_background',
      'Health and Lifestyle': 'health_lifestyle',
      'Passport Info': 'passport_info',
      'Program Preference': 'program_preference',
      'My Description': 'my_description'
    }
  },
  volunteer: {
    'Home': {
      'Home': 'dashboard'
    },
    'Manage': {
      'Host families': 'host_families',
      'Departing Students': 'students',
      'Associate Requests': 'associate_requests',
      'Posts': 'posts'
    }
  },
  family: {
    'Rollbar': {
      'Home': 'dashboard',
      'My Request': 'my_journey_request',
      'Posts': 'posts'
    },
    'My Infos': {
      'Health and Lifestyle': 'health_lifestyle'
      // maybe the above routes might collide with
      // student's one in the backend
    }
  }
}
